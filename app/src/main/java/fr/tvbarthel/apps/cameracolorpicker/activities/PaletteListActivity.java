package fr.tvbarthel.apps.cameracolorpicker.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.melnykov.fab.FloatingActionButton;

import fr.tvbarthel.apps.cameracolorpicker.R;
import fr.tvbarthel.apps.cameracolorpicker.data.ColorItems;
import fr.tvbarthel.apps.cameracolorpicker.views.PaletteListPage;

/**
 * Created by Lenovo on 22-05-2017.
 */

public class PaletteListActivity extends BaseActivity implements PaletteListPage.Listener{

    private PaletteListPage mPaletteListPage;
    private FrameLayout frameLayoutPlatteList;
    private FloatingActionButton activity_main_fab;
    private Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_platte_list);

        mContext = this;
        frameLayoutPlatteList = (FrameLayout) findViewById(R.id.frameLayoutPlatteList);
        activity_main_fab = (FloatingActionButton) findViewById(R.id.activity_main_fab);

        mPaletteListPage = new PaletteListPage(this);
        mPaletteListPage.setListener(this);

        frameLayoutPlatteList.addView(mPaletteListPage);

        activity_main_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ColorItems.getSavedColorItems(mContext).size() <= 1) {
                    showToast(R.string.activity_main_error_not_enough_colors);
                } else {
                    final Intent intentColorPaletteActivity = new Intent(mContext, PaletteCreationActivity.class);
                    startActivity(intentColorPaletteActivity);
                }
            }
        });
    }

    @Override
    public void onEmphasisOnPaletteCreationRequested() {

    }
}
