package fr.tvbarthel.apps.cameracolorpicker.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jrummyapps.android.colorpicker.ColorPickerDialog;
import com.jrummyapps.android.colorpicker.ColorPickerDialogListener;
import com.melnykov.fab.FloatingActionButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.wada811.android.material.design.colors.sample.ItemListActivity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import fr.tvbarthel.apps.cameracolorpicker.R;
import fr.tvbarthel.apps.cameracolorpicker.fragments.AboutDialogFragment;
import fr.tvbarthel.apps.cameracolorpicker.views.ColorItemListPage;
import fr.tvbarthel.apps.cameracolorpicker.views.PaletteListPage;


/**
 * An {@link android.support.v7.app.AppCompatActivity} that shows the list of the colors that the user saved.
 * <p/>
 */
public class MainActivity extends BaseActivity implements ColorItemListPage.Listener,
        ColorPickerDialogListener {

    @IntDef({PAGE_ID_COLOR_ITEM_LIST, PAGE_ID_PALETTE_LIST})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PageId {
    }

    /**
     * The id associated with the color item list page.
     */
    private static final int PAGE_ID_COLOR_ITEM_LIST = 1;

    /**
     * The id associated with the palette list page.
     */
    private static final int PAGE_ID_PALETTE_LIST = 2;

    /**
     * The {@link Toolbar} of this {@link MainActivity}.
     */
    private Toolbar mToolbar;
    /**
     * A {@link ViewPager} that displays two pages: {@link ColorItemListPage} and {@link PaletteListPage}.
     */
    private ViewPager mViewPager;

    /**
     * The {@link ColorItemListPage} being displayed in the {@link ViewPager}.
     */
    private ColorItemListPage mColorItemListPage;

    /**
     * The id of the current page selected.
     * <p/>
     * {@link fr.tvbarthel.apps.cameracolorpicker.activities.MainActivity.PageId}
     * Used for updating the icon of the {@link FloatingActionButton} when the user scrolls between pages.
     */
    private int mCurrentPageId;
    private BoomMenuButton bmb;
    private TextView txtToolbarTitle;
    private FrameLayout frameContainer;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        mToolbar.setContentInsetsAbsolute(0,0);
        setSupportActionBar(mToolbar);

        txtToolbarTitle = (TextView) findViewById(R.id.txtToolbarTitle);
        txtToolbarTitle.setText(getString(R.string.app_name));
        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        setMenuButtons();

        mCurrentPageId = PAGE_ID_COLOR_ITEM_LIST;
        mColorItemListPage = new ColorItemListPage(this);
        mColorItemListPage.setListener(this);



        frameContainer = (FrameLayout) findViewById(R.id.frameContainer);
        frameContainer.addView(mColorItemListPage);

    }

    private void setMenuButtons(){
        String[] menuText = getResources().getStringArray(R.array.menu_names);

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .normalText(menuText[i])
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            switch (index){
                                case 0:
                                    Intent intentColorPickerActivity = new Intent(mContext, ColorPickerActivity.class);
                                    startActivity(intentColorPickerActivity);
                                    break;

                                case 1:
                                    Intent intentImageColorPicActivity = new Intent(mContext, ImageColorPickerActivity.class);
                                    startActivity(intentImageColorPicActivity);
                                    break;

                                case 2:
                                    ColorPickerDialog.newBuilder()
                                            .setDialogType(ColorPickerDialog.TYPE_CUSTOM)
                                            .setAllowPresets(false)
                                            .setDialogId(100)
                                            .setColor(Color.BLACK)
                                            .setShowAlphaSlider(true)
                                            .show(MainActivity.this);
                                    break;

                                case 3:
                                    Intent intentColorListActivity = new Intent(mContext, ItemListActivity.class);
                                    startActivity(intentColorListActivity);
                                    break;

                                case 5:
                                    Intent intentPaletteListActivity = new Intent(mContext, PaletteListActivity.class);
                                    startActivity(intentPaletteListActivity);
                                    break;
                            }
                        }
                    });
            bmb.addBuilder(builder);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Inflate the menu specific to the flavor.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        boolean handled = false;

        switch (itemId) {
            case R.id.menu_main_action_licenses:
                handled = true;
                final Intent intent = new Intent(this, LicenseActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_main_action_about:
                handled = true;
                AboutDialogFragment.newInstance().show(getSupportFragmentManager(), null);
                break;

            case R.id.menu_main_action_contact_us:
                handled = true;
                final String uriString = getString(R.string.contact_us_uri,
                        Uri.encode(getString(R.string.contact_us_email)),
                        Uri.encode(getString(R.string.contact_us_default_subject)));
                final Uri mailToUri = Uri.parse(uriString);
                final Intent sendToIntent = new Intent(Intent.ACTION_SENDTO);
                sendToIntent.setData(mailToUri);
                startActivity(sendToIntent);
                break;
        }

        return handled;
    }

    @Override
    public void onEmphasisOnAddColorActionRequested() {

    }


    @Override
    public void onColorSelected(int dialogId, @ColorInt int color) {
        Log.e("selected color", Integer.toHexString(color) + "");
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }
}
