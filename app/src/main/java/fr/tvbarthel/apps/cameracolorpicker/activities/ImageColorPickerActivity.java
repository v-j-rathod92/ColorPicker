package fr.tvbarthel.apps.cameracolorpicker.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.skydoves.colorpickerview.ColorPickerView;

import fr.tvbarthel.apps.cameracolorpicker.R;

/**
 * Created by Lenovo on 22-05-2017.
 */

public class ImageColorPickerActivity extends BaseActivity implements View.OnClickListener{
    private ColorPickerView colorPickerView;
    private View fabBgView;
    private FloatingActionButton menu_item, menu_item1;
//    private FloatingActionMenu floatingActionMenu;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_image_color_picker);

//        fabBgView = findViewById(R.id.fab_bg);
//        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.menu);
        menu_item = (FloatingActionButton) findViewById(R.id.menu_item);
        menu_item1 = (FloatingActionButton) findViewById(R.id.menu_item1);

//        floatingActionMenu.setOnClickListener(this);
        menu_item.setOnClickListener(this);
        menu_item1.setOnClickListener(this);

//        floatingActionMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
//            @Override
//            public void onMenuToggle(boolean opened) {
//                if (opened) revealShow(fabBgView, true);
//                else revealShow(fabBgView, false);
//            }
//        });

        colorPickerView = (ColorPickerView) findViewById(R.id.colorPickerView);
        colorPickerView.setColorListener(new ColorPickerView.ColorListener() {
            @Override
            public void onColorSelected(int color) {
                Log.e("color", color + "");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.menu_item:
                revealShow(fabBgView, false);
//                floatingActionMenu.close(true);
                break;

            case R.id.menu_item1:
                revealShow(fabBgView, false);
//                floatingActionMenu.close(true);
                break;

        }
    }
}
